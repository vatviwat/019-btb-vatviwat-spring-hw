package com.viwat.homework.configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests().anyRequest().authenticated().and().httpBasic();
    }
    @Autowired
    public void configreGlobal(AuthenticationManagerBuilder authentication) throws Exception{
        authentication.inMemoryAuthentication().withUser("user").password("{noop}user@123").roles("USER");
        authentication.inMemoryAuthentication().withUser("admin").password("{noop}admin@123").roles("ADMIN","USER");

    }

}
