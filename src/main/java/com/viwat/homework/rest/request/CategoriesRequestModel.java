package com.viwat.homework.rest.request;

public class CategoriesRequestModel {

    private String title;

    public CategoriesRequestModel(){}

    public CategoriesRequestModel( String title) {
        this.title = title;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "CategoriesRequestModel{" +
                ", title='" + title + '\'' +
                '}';
    }
}
