package com.viwat.homework.rest.controller;

import com.viwat.homework.repository.dto.BooksDto;
import com.viwat.homework.repository.dto.CategoriesDto;
import com.viwat.homework.rest.request.BooksRequestModel;
import com.viwat.homework.rest.request.CategoriesRequestModel;
import com.viwat.homework.rest.response.BaseApiResponse;
import com.viwat.homework.rest.response.CategoryResponse;
import com.viwat.homework.service.imp.CategoriesServiceImp;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
@RestController
@RequestMapping("/api")
public class CategoriesController {
    private CategoriesServiceImp categoriesServiceImp;
    @Autowired
    public CategoriesController(CategoriesServiceImp categoriesServiceImp) {
        this.categoriesServiceImp = categoriesServiceImp;
    }
    @PostMapping("/categories")
    // @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<BaseApiResponse<CategoriesRequestModel>> insert(
            @RequestBody CategoriesRequestModel categories) {
        BaseApiResponse<CategoriesRequestModel> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        CategoriesDto categoriesDto = mapper.map(categories, CategoriesDto.class);
        CategoriesDto result = categoriesServiceImp.insert(categoriesDto);
        CategoriesRequestModel result2 = mapper.map(result, CategoriesRequestModel.class);
        response.setMessage("You have added category successfully");
        response.setData(result2);
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }


    @GetMapping("/categories")
    // @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    public ResponseEntity<BaseApiResponse<List<CategoryResponse>>> select() {

        ModelMapper mapper = new ModelMapper();
        BaseApiResponse<List<CategoryResponse>> response =
                new BaseApiResponse<>();

        List<CategoriesDto> categoriesDtoList = categoriesServiceImp.select();
        List<CategoryResponse> categoryResponses = new ArrayList<>();

        for (CategoriesDto categoriesDto : categoriesDtoList) {
            categoryResponses.add(mapper.map(categoriesDto, CategoryResponse.class));
        }

        response.setMessage("You have found all categories successfully");
        response.setData(categoryResponses);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/categories/delete/{id}")
    // @PreAuthorize("hasRole('ROLE_ADMIN')")
    private void deleteCategory(@PathVariable("id") int id) {
        categoriesServiceImp.deleteCategory(id);
    }

    @PutMapping("/categories/update/{id}")
    // @PreAuthorize("hasRole('ROLE_ADMIN')")
    private void updateCategory(@PathVariable("id") int id,@RequestBody CategoriesDto categoriesDto){
        categoriesServiceImp.updateCategory(id,categoriesDto.getTitle());
    }
    @GetMapping("categories/pagination")
    // @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    public ResponseEntity<List<CategoriesDto>> getByPagination(@RequestParam int page, @RequestParam int recordNum){
        return new ResponseEntity<>(categoriesServiceImp.getByPagination(page, recordNum), HttpStatus.OK);
    }
}

