package com.viwat.homework.rest.controller;
import com.viwat.homework.repository.dto.BooksDto;
import com.viwat.homework.repository.dto.CategoriesDto;
import com.viwat.homework.rest.request.BooksRequestModel;
import com.viwat.homework.rest.request.CategoriesRequestModel;
import com.viwat.homework.rest.response.BaseApiResponse;
import com.viwat.homework.service.imp.BooksServiceImp;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Book;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
@RestController
@RequestMapping("/api")
public class BooksController {
    private BooksServiceImp booksServiceImp;
    @Autowired
    public BooksController(BooksServiceImp booksServiceImp) {
        this.booksServiceImp = booksServiceImp;
    }

    @GetMapping("/books")
    // @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    public ResponseEntity<BaseApiResponse<List<BooksRequestModel>>> select() {

        ModelMapper mapper = new ModelMapper();
        BaseApiResponse<List<BooksRequestModel>> response =
                new BaseApiResponse<>();

        List<BooksDto> booksDtoList = booksServiceImp.select();
        List<BooksRequestModel> booksRequestModels= new ArrayList<>();

        for (BooksDto booksDto : booksDtoList) {
            booksRequestModels.add(mapper.map(booksDto, BooksRequestModel.class));
        }

        response.setMessage("You have found all books successfully");
        response.setData(booksRequestModels);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }
    @PostMapping("/books")
    // @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<BaseApiResponse<BooksRequestModel>> insert(
            @RequestBody BooksRequestModel books) {

        BaseApiResponse<BooksRequestModel> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        BooksDto booksDto = mapper.map(books, BooksDto.class);
        BooksDto result = booksServiceImp.insert(booksDto);
        BooksRequestModel result2 = mapper.map(result, BooksRequestModel.class);
        response.setMessage("You have added book successfully");
        response.setData(result2);
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);

    }
    @DeleteMapping("/books/{id}")
//    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    private void deleteBook(@PathVariable("id") int id) {
        booksServiceImp.deleteBook(id);
    }
    @PutMapping("/books/update/{id}")
    // @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    private void updateBook(@PathVariable("id") int id,@RequestBody BooksDto booksDto){
        booksServiceImp.updateBook(id,booksDto.getTitle(),booksDto.getAuthor(),booksDto.getDescription(),booksDto.getThumbnail());
    }
    @GetMapping("/books/find/{id}")
    // @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    private ResponseEntity<BaseApiResponse<List<BooksRequestModel>>> findOne(@PathVariable("id") int id){
        ModelMapper mapper = new ModelMapper();
        BaseApiResponse<List<BooksRequestModel>> response = new BaseApiResponse<>();
        System.out.println("found");
        List<BooksDto> booksDtoList = booksServiceImp.findOne(id);
        List<BooksRequestModel> booksRequestModels= new ArrayList<>();

        for (BooksDto booksDto : booksDtoList) {
            booksRequestModels.add(mapper.map(booksDto, BooksRequestModel.class));
        }
        response.setMessage("You have found a book successfully");
        response.setData(booksRequestModels);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }

    @GetMapping("/books/search/{title}")
    // @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    private ResponseEntity<BaseApiResponse<List<BooksRequestModel>>> findOne(@PathVariable("title") String title){
        ModelMapper mapper = new ModelMapper();
        BaseApiResponse<List<BooksRequestModel>> response = new BaseApiResponse<>();
        System.out.println("found");
        List<BooksDto> booksDtoList = booksServiceImp.filterByTitle(title);
        List<BooksRequestModel> booksRequestModels= new ArrayList<>();

        for (BooksDto booksDto : booksDtoList) {
            booksRequestModels.add(mapper.map(booksDto, BooksRequestModel.class));
        }
        response.setMessage("You have found a book successfully by title");
        response.setData(booksRequestModels);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }

    @GetMapping("books/pagination")
    // @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    public ResponseEntity<List<BooksDto>> getByPagination(@RequestParam int page, @RequestParam int recordNum){
        return new ResponseEntity<>(booksServiceImp.getByPagination(page, recordNum), HttpStatus.OK);
    }
}
