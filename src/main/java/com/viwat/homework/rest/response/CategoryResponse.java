package com.viwat.homework.rest.response;

public class CategoryResponse {
    private int id;
    private String title;

    public CategoryResponse(){}
    public CategoryResponse(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "CategoryResponse{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}
