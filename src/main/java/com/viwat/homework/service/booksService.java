package com.viwat.homework.service;

import com.viwat.homework.Pagination;
import com.viwat.homework.repository.dto.BooksDto;
import com.viwat.homework.repository.dto.CategoriesDto;

import java.util.List;

public interface booksService {
    BooksDto insert(BooksDto booksDto);
    List<BooksDto> select();
    void deleteBook(int id);
    void updateBook(int id,String title,String author,String description,String thumbnail);
    List<BooksDto> findOne(int id);
    List<BooksDto> filterByTitle(String title);
    List<BooksDto> getBooksTitleById(int id);
    List<BooksDto> getByPagination(int page, int recordNum);
}
