package com.viwat.homework.service;

import com.viwat.homework.repository.dto.BooksDto;
import com.viwat.homework.repository.dto.CategoriesDto;

import java.util.List;

public interface categoryService {
    CategoriesDto insert(CategoriesDto categoriesDto);
    List<CategoriesDto> select();
    void deleteCategory(int id);
    void updateCategory(int id,String title);
    List<CategoriesDto> getByPagination(int page, int recordNum);
}
