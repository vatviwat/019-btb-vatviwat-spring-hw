package com.viwat.homework.service.imp;

import com.viwat.homework.Pagination;
import com.viwat.homework.repository.BooksRepository;
import com.viwat.homework.repository.dto.BooksDto;
import com.viwat.homework.service.booksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
@Service
public class BooksServiceImp implements booksService {
    private BooksRepository booksRepository;
    @Autowired
    public BooksServiceImp(BooksRepository booksRepository) {
        this.booksRepository = booksRepository;
    }

    @Override
    public BooksDto insert(BooksDto booksDto) {
        boolean isInserted = booksRepository.insert(booksDto);
        if (isInserted) {
            return booksDto;
        }
        else
            return null;
    }

    @Override
    public List<BooksDto> select() {
        return booksRepository.select();
    }

    @Override
    public void deleteBook(int id) {
        booksRepository.deleteBook(id);
    }

    @Override
    public void updateBook(int id, String title, String author, String description, String thumbnail) {
        booksRepository.updateBook(id,title,author,description,thumbnail);
    }

    @Override
    public List<BooksDto> findOne(int id) {
        return booksRepository.findOne(id);
    }

    @Override
    public List<BooksDto> filterByTitle(String title) {
        return booksRepository.filterByTitle(title);
    }

    @Override
    public List<BooksDto> getBooksTitleById(int id) {
        return booksRepository.getBooksTitleById(id);
    }

    @Override
    public List<BooksDto> getByPagination(int page, int recordNum) {
        return booksRepository.getByPagination(page-1,recordNum);
    }

}
