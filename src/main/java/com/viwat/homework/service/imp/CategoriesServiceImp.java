package com.viwat.homework.service.imp;

import com.viwat.homework.repository.CategoriesRepository;
import com.viwat.homework.repository.dto.BooksDto;
import com.viwat.homework.repository.dto.CategoriesDto;
import com.viwat.homework.service.categoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
@Service
public class CategoriesServiceImp implements categoryService {
    private CategoriesRepository categoriesRepository;

    @Autowired
    public CategoriesServiceImp(CategoriesRepository categoriesRepository) {
        this.categoriesRepository = categoriesRepository;
    }

    @Override
    public CategoriesDto insert(CategoriesDto categories) {
        boolean isInserted = categoriesRepository.insert(categories);
        if (isInserted)
            return categories;
        else
            return null;
    }

    @Override
    public List<CategoriesDto> select() {
        return categoriesRepository.select();
    }

    @Override
    public void deleteCategory(int id) {
        categoriesRepository.deleteCategory(id);
    }

    @Override
    public void updateCategory(int id,String title) {
        categoriesRepository.updateCategory(id,title);
    }

    @Override
    public List<CategoriesDto> getByPagination(int page, int recordNum) {
        return categoriesRepository.getByPagination(page-1,recordNum);
    }

}
