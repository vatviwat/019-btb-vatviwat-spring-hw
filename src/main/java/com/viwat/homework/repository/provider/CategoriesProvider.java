package com.viwat.homework.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class CategoriesProvider {
    public String select() {
        return new SQL(){{
            SELECT("*");
            FROM("tb_category");
        }}.toString();
    }

    public String selectByFilter(int id, String title) {
        return new SQL(){{
            SELECT("*");
            FROM("tb_category");

            if (id != 0)
                WHERE("id = #{id}");
            else if (title != null || !title.equals(""))
                WHERE("title = #{title}");
            else
                WHERE();

        }}.toString();
    }


    public String deleteCategory(int id){
        return new SQL(){{
            DELETE_FROM("tb_category");
            WHERE("id = #{id}");
        }}.toString();
    }

    public String updateCategory(int id,String title){
        return new SQL(){{
            UPDATE("tb_category");
            SET("title = #{title}");
            WHERE("id = #{id}");
        }}.toString();
    }

}
