package com.viwat.homework.repository.provider;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.jdbc.SQL;

public class BooksProvider {
    public String select() {
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
        }}.toString();
    }

    public String selectByFilter(int id, String title) {
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            if (id != 0)
                WHERE("id = #{id}");
            else if (title != null || !title.equals(""))
                WHERE("title = #{title}");
            else
                WHERE();

        }}.toString();
    }
    public String deleteBook(int id){
        return new SQL(){{
            DELETE_FROM("tb_books");
            WHERE("id = #{id}");
        }}.toString();
    }
    public String updateBook(int id,String title,String author,String description,String thumbnail){
        return new SQL(){{
            UPDATE("tb_books");
            SET("author = #{author}, description = #{description}, thumbnail = #{thumbnail}");
            WHERE("id = #{id}");
        }}.toString();
    }
    public String findOne(int id){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            WHERE("id = #{id}");
        }}.toString();
    }
    public String filterByTitle(String title){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            WHERE("title LIKE CONCAT('${title}', '%')");
        }}.toString();
    }
    public String getBooksTitleById(int id){
        return new SQL(){{
            SELECT("title");
            FROM("tb_category");
            WHERE("id = #{id}");
        }}.toString();
    }
}
