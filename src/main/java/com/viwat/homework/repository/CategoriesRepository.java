package com.viwat.homework.repository;

import com.viwat.homework.repository.dto.BooksDto;
import com.viwat.homework.repository.dto.CategoriesDto;
import com.viwat.homework.repository.provider.BooksProvider;
import com.viwat.homework.repository.provider.CategoriesProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.List;
@Repository
public interface CategoriesRepository {
    @Insert("INSERT INTO tb_category (title) VALUES (#{title})")
    boolean insert(CategoriesDto categoriesDto);

    @SelectProvider(value = CategoriesProvider.class, method = "select")
    List<CategoriesDto> select();

    @DeleteProvider(value = CategoriesProvider.class,method = "deleteCategory")
    void deleteCategory(int id);

    @UpdateProvider(value = CategoriesProvider.class,method = "updateCategory")
    void updateCategory(int id,String title);

    @Select("SELECT * FROM tb_category limit #{recordNum} offset #{page}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
    })
    List<CategoriesDto> getByPagination(int page, int recordNum);
}
