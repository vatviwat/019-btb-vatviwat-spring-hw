package com.viwat.homework.repository;

import com.viwat.homework.Pagination;
import com.viwat.homework.repository.dto.BooksDto;
import com.viwat.homework.repository.dto.CategoriesDto;
import com.viwat.homework.repository.provider.BooksProvider;
import com.viwat.homework.repository.provider.CategoriesProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BooksRepository {
    @Insert("INSERT INTO tb_books (title, author, description, thumbnail, category_id) " +
            "VALUES (#{title}, #{author}, #{description}, #{thumbnail}, #{category_id})")
    boolean insert(BooksDto booksDto);

    @SelectProvider(value = BooksProvider.class, method = "select")
    List<BooksDto> select();

    @DeleteProvider(value = BooksProvider.class,method = "deleteBook")
    void deleteBook(int id);

    @UpdateProvider(value = BooksProvider.class,method = "updateBook")
    void updateBook(int id,String title,String author,String description,String thumbnail);

    @SelectProvider(value = BooksProvider.class,method = "findOne")
    List<BooksDto> findOne(int id);

    @SelectProvider(value = BooksProvider.class,method = "filterByTitle")
    List<BooksDto> filterByTitle(String title);

    @SelectProvider(value = BooksProvider.class,method = "getBooksTitleById")
    List<BooksDto> getBooksTitleById(int id);

    @Select("SELECT * FROM tb_books limit #{recordNum} offset #{page}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "author", column = "author"),
            @Result(property = "description", column = "description"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "category_id", column = "category_id"),
    })

    List<BooksDto> getByPagination(int page, int recordNum);

}
